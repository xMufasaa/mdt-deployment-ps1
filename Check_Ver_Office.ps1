# This script verifies the OS and version of Office that is installed on a device and stores it in the %OfficeVer% variable
# If Office is not installed, the content of the variable is "Not Installed"
# Author Sean Young
# Rev 1.0
# Oct 2019

Clear-Host

# Set default string for OfficeVer variable
$global:OfficeVer="Not Installed"

# Check bitness of Windows by checking for a Program Files (x86) Environment Variable. Only 64-bit Windows will have this.
# IF NOT "%ProgramFiles(x86)%"=="" (goto query64) else (goto query86)
if ([System.Environment]::Is64BitOperatingSystem) {
    query64
}
else {
    query86
}

# Operating system is x64. Verify latest version of Office installed; confirm bitness
function query64 {
    # Check for Office 2016 ProPlus 32-bit installation
    if (Test-Path "HKLM:\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office16.PROPLUS") {
        $global:OfficeVer="2016 x86 ProPlus"
    }
    # Check for Office 2013 ProPlus 32-bit installation
    if (Test-Path "HKLM:\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.PROPLUS") {
        $global:OfficeVer="OfficeVer=2013 x86 ProPlus"
    }
    # Check for Office 2013 Standard 32-bit installation
    if (Test-Path "HKLM:\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.STANDARD") {
        $global:OfficeVer="OfficeVer=2013 x86 Standard"
    }
    # Check for Office 2010 Standard 32-bit installation
    if (Test-Path "HKLM:\SOFTWARE\WOW6432NODE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.STANDARD") {
        $global:OfficeVer="OfficeVer=2010 x86 Standard"
    }
    # Check for Office 2016 ProPlus 64-bit installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office16.PROPLUS") {
        $global:OfficeVer="OfficeVer=2016 x64 ProPlus"
    }
    # Check for Office 2013 ProPlus 64-bit installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.PROPLUS") {
        $global:OfficeVer="OfficeVer=2013 x64 Standard"
    }
    # Check for Office 2013 Standard 64-bit installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.STANDARD") {
        $global:OfficeVer="OfficeVer=2013 x64 Standard"
    }
    # Check for Office 2010 Standard 64-bit installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.STANDARD") {
        $global:OfficeVer="OfficeVer=2010 x64 Standard"
    }
}

# Operating System is x86. Verify latest version of Office installed
function query86 {
    # Check for Office 2016 ProPlus installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office16.PROPLUS") {
        $global:OfficeVer="OfficeVer=2016 x86 ProPlus"
    }
    # Check for Office 2013 ProPlus installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.PROPLUS") {
        $global:OfficeVer="OfficeVer=2013 x86 ProPlus"
    }
    # Check for Office 2013 Standard installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office15.STANDARD") {
        $global:OfficeVer="OfficeVer=2013 x86 Standard"
    }
    # Check for Office 2010 Standard installation
    if (Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Office14.STANDARD") {
        $global:OfficeVer="OfficeVer=2010 x86 Standard"
    }
}





