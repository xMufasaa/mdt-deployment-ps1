<#
This script checks the OS Version on a device and stores it in the $global:OSVer variable
Author Sean Young
Rev 1.0
Oct 2019
#>

Clear-Host

# Retrieve OS version
$winVer=(Get-WmiObject Win32_OperatingSystem).Version

# Retrieve OS bitness
# if not "%ProgramFiles(x86)%"=="" (goto query64) else (goto query86)
if ([System.Environment]::Is64BitOperatingSystem) {
    query64
}
else {
    query86
}

# Associate $global:OSVer with its appropriate name; 64-bit
function query64 {}
# Windows 10
if ($winVer -match '^\d10\.') {
    $global:OSVer="Windows 10 x64"
}
# Windows 8.1
if ($winVer -match '^\d6\.3') { 
    $global:OSVer="Windows 8.1 x64"
}
# Windows 8
if ($winVer -match '^\d6\.2') { 
    $global:OSVer="Windows 8 x64"
}
# Windows 7
if ($winVer -match '^\d6\.1') { 
    $global:OSVer="Windows 7 x64"
}
# Windows Vista
if ($winVer -match '^\d6\.0') { 
    $global:OSVer="Windows Vista x64"
}
# Windows XP
if ($winVer -match '^\d5\.2') { 
    $global:OSVer="Windows XP x64"
}
exit /b

# Associate $global:OSVer with its appropriate name; 32-bit
function query86 {}
# Windows 10
if ($winVer -match '^\d10\.') {
    $global:OSVer="Windows 10 x86"
}
# Windows 8.1
if ($winVer -match '^\d6\.3') { 
    $global:OSVer="Windows 8.1 x86"
}
# Windows 8
if ($winVer -match '^\d6\.2') { 
    $global:OSVer="Windows 8 x86"
}
# Windows 7
if ($winVer -match '^\d6\.1') { 
    $global:OSVer="Windows 7 x86"
}
# Windows Vista
if ($winVer -match '^\d6\.0') { 
    $global:OSVer="Windows Vista x86"
}
# Windows XP
if ($winVer -match '^\d5\.2') { 
    $global:OSVer="Windows XP x64"
}