<# 
This is an Application Deployment Template
Author Sean Young
Rev 1.0
Oct 2019

This template should work for a large majority of application deployments just by editing the
variables under the EDIT THESE VARIABLES heading in the Setup section. On rare occasions,
changes may need to be made to the installation switches under :install86 and :install64.
The defaults are "ACCEPT_EULA=1 /quiet /norestart" for .msi installs and "/verysilent" for .exe installs
#>

Clear-Host

# *********************************************************************
# Setup;
# *********************************************************************
# Declare Variables

# Set $MDT 
#if (Test-Path "\\SERVER\deploymentshare$\scripts\PSsetMDT.ps1") {
    \\SERVER\deploymentshare$\scripts\PSsetMDT.ps1
}


# If $MDT is not defined, set to local path; Output $MDT
if (!(Test-Path variable:global:MDT)) {
    $global:MDT=$(Get-Location)
}
Write-Output "MDT location: $global:MDT"

############# EDIT THESE VARIABLES ###########
# Set the application name, spaces are optional; This will be used for creating the log directory
$appName="Application_Name"

# Choose the application bitness, if applicable; x86 applications can install on x64 systems, but not vice-versa
# This is not needed if installing by Office version, as that will dictate the application bitness
# [Un-comment one, comment the other]
$appBitness="x64"
# $appBitness="x86"

# Choose to Deploy by OS version/bitness or by Office version/bitness
# Installing by Office version will FAIL if Office is not installed
# [Un-comment one, comment the other]
$deployBy="OS"
# $deployBy="Office"

# Choose to Install with .msi or .exe
# [Un-comment one, comment the other]
$installType="msi"
# $installType="exe"

# Set the path to the application's x86 and x64 .msi/.exe, if applicable, including the .msi/.exe itself with no switches
$installPathx86="$global:MDT\App_Folder\App_installer.msi"
$installPathx64="$global:MDT\App_Folder\App_installer.msi"

# Set local path of the application, this is used to see if it is already installed on the target machine
# This is likely in %ProgramFiles% or %ProgramFilesx86%, but could be elsewhere for non-standard or per-user installs 
$localPath="$env:ProgramFiles(x86)\App_Folder\Launcher.exe"
##############################################

# Set the logFile variable
$logFile="\\SERVER\DeploymentLogs$\$appName"

# *********************************************************************
# Create Functions; You shouldn't have to edit anything below this line
# *********************************************************************

# ***** Deployment 1: x86 or x64 deployment by OS version/bitness *****
function deployByOS {
    # Verify OS version and bitness
    call \\SERVER\deploymentshare$\scripts\Check_Ver_OS.bat

    # Read $global:OSVer from the above script; if x64, read $appBitness
    if ($global:OSVer | Select-String -Pattern "x86") {
        install86 
    }
    elseif ($appBitness | Select-String -Pattern "x64") {
        install64 
    } 
    else {
        install86 
    }
}
# ******************************************************

# ***** Deployment 2: x86 or x64 deployment by Office version/bitness *****
function deployByOffice {
    # Verify latest version and bitness of Office installed
    call \\SERVER\deploymentshare$\scripts\Check_Ver_Office.bat

    # Read $global:OfficeVer from the above script
    if ($global:OfficeVer | Select-String -Pattern "Not") {
        ERR 
    }
    elseif ($global:OfficeVer | Select-String -Pattern "x64") {
        install64 
    }
    else {
        install86 
    } 
}
# **********************************************************

function install86 {
    # Install x86 Application; Select msi or exe deployment based on $installType
    Write-Output Install Application
    if ($installType | Select-String -Pattern "msi") {
        Start-Process msiexec.exe -Wait -ArgumentList "/i $installPathx86 ACCEPT_EULA=1 /quiet /norestart"
    } 
    else {
        Start-Process $installPathx86 -Wait -ArgumentList "/verysilent"
    }
    if ($LastExitCode -ne "1") {
        DONE
    }  
    else {
        ERR 
    }
}

function install64 {
    # Install x64 Application; Select msi or exe deployment based on $installType
    Write-Output Install Application
    if ($installType | Select-String -Pattern "msi") {
        Start-Process msiexec.exe -Wait -ArgumentList "/i $installPathx64 ACCEPT_EULA=1 /quiet /norestart"
    } 
    else {
        Start-Process $installPathx64 -Wait -ArgumentList "/verysilent"
    }
    if ($LastExitCode -ne "1") {
        DONE
    }  
    else {
       ERR 
    }
}

function SKIP {
    # Add entry to log file if already installed
    Write-Output %date% %time% %username% %appName% is already installed on device %computername% | Out-File -Append $logFile\log.txt
    exit /b
}

function ERR {
    # Add entry to log file for failure
    Write-Output %date% %time% %username% %appName% was not installed on device %computername%, or another error occurred | Out-File -Append $logFile\log.txt
    exit /b
}

function DONE {
    # Add entry to log file for success
    Write-Output %date% %time% %username% %appName% successfully installed on device %computername% | Out-File -Append $logFile\log.txt
}

# *********************************************************************
# Begin deployment; 
# *********************************************************************

# Review variables
Write-Output "Application Name: $appName"
if ($deployBy -eq "OS" -and $appBitness -eq "x64") {
    Write-Host "Application Installer: $installPathx64" 
}
elseif ($deployBy -eq "OS" -and $appBitness -eq "x86") {
    Write-Host "Application Installer: $installPathx86" 
}
Write-Output "Application Bitness: $appBitness"
Write-Output "Install by: $deployBy version"
Write-Output "Install type: .$installType"
Write-Output "Local Install Path: $localPath"
Write-Output "Log Path: $logFile"

# If application folder does not exist in DeploymentLogs$, create it
if (!(Test-Path "$logFile")) {
    New-Item -Path $logFile -ItemType Directory 
}

# Error-checking; If the program is already installed, skip installation
if (Test-Path "$localPath") {
    SKIP
}

# Read $deployBy to determine method of install and call the appropriate one
if ($deployBy | Select-String -Pattern "OS") {
    deployByOS
}
else {
    deployByOffice
}