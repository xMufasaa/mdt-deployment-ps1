# This script sets location of MDT application share based on Default Gateway
# Author Sean Young
# Rev 1.0
# Oct 2019

Clear-Host

# Read DefaultIPGateway from WMI and store in $gateway;
$gateway = (Get-wmiObject Win32_networkAdapterConfiguration | Where-Object {$_.IPEnabled}).DefaultIPGateway

# Regex search for ".x." in Gateway

# 10."1".x.x
if ($gateway -match '^\d+\.1\.') {
    $global:MDT=\\SERVER\Deploymentshare$\applications
}

# 10."2".x.x
if ($gateway -match '^\d+\.2\.') {
    $global:MDT=\\SERVER2\Deploymentshare$\applications
}

# 10."3".x.x
if ($gateway -match '^\d+\.3\.') {
    $global:MDT=\\SERVER3\Deploymentshare$\applications
}

# Repeat as necessary for other networks/subnets
